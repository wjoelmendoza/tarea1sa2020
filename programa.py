#!/usr/bin/env python3
import requests
import json

url = 'https://api.softwareavanzado.world/index.php'


def login():
    '''
    Esta se encarga de solictar el token de autorizacion
    para realizar las operaciones de listar y crear contactos
    '''
    # opción y tipo de api
    params_oauth2_l = {
        'option': 'token',
        'api': 'oauth2'
    }

    # credenciales
    data_oauth2 = {
        'grant_type': 'client_credentials',
        'client_id': 'wjoelmendoza@gmail.com',
        'client_secret': '201212680'
    }

    # haciendo el request del token
    req = requests.post(url, params=params_oauth2_l, data=data_oauth2)
    datos_auth = req.json()
    return datos_auth


def lista(token):
    '''
    Se encarga de consumir el servicio para listar los contactos
    luego los muestra en pantalla, recibe como parametro el token de
    autenticación que el servidor requiere para poder brindar la información
    '''
    # parametros para
    paramsb = {
        'access_token': token,
        'webserviceVersion': '1.0.0',
        'option': 'com_contact',
        'api': 'Hal',
        'webserviceClient': 'administrator',
        'list[limit]': '0'
    }

    req = requests.get(url, params=paramsb)
    resultado = req.json()
    res = resultado['_embedded']
    items = res['item']
    print('contactos:')
    strval = json.dumps(items, indent=4)
    print(strval)


def crear(token, num):
    '''
    Se encarga de la creación de nuevos contactos, recibe un token autorizado
    del api Rest
    '''
    # parametros para la creacion de nuevos contactos
    params = {
        'access_token': token,
        'webserviceVersion': '1.0.0',
        'option': 'contact',
        'api': 'hal',
        'webserviceClient': 'administrator'
    }

    idt = '201212680_Walter' + str(num)

    # datos de creación de nuevo contacto
    data = {
        'name': idt,
        'catid': 1
    }

    # realizando peticion
    req = requests.post(url, params=params, data=data)
    rst = req.json()

    # validando si existio error
    if rst['result']:
        print("id: " + str(rst['id']) + ", resultado: " + str(rst['result']))
    else:
        print('error')


def inicio():
    '''
    función principal se encarga de llamar a la función que genera el token
    y se encarga de crear 10 usuarios
    '''
    # generando token
    dato = login()
    token = dato['access_token']
    print(token)

    # listando contactos
    lista(token)

    # creando usuarios
    for i in range(10):
        crear(token, i)


# ejecutando script
if __name__ == '__main__':
    inicio()
    print("201212680 Walter Mendoza")
