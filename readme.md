# Tarea1

Este proyecto consume un api rest con oAuth2, para lo cual debe de obtener un token de autorización mediante un client secret.

Este cliente lista todos los contactos que tiene el servidor y de igual forma crea 10 nuevos registros

Se compone de las siguientes funciones:
* login
* lista
* crear
* inicio

## login
Esta función se encarga de solicitar el token de autenticación al servidor
y retorna dicho token

## lista(token)
Esta función recibe como parámetro el token para poder consumir el recurso de 
listar contactos, enviando las correspondientes opciones al api, una vez que
obtiene dicho listado los despliega en la salida estándar del sistema.

## crear(token, num)
Esta función recibe como parámetro token para poder consumir el recurso de 
creación de usuarios, num es un parámetro que se concatena al nombre de usuario
para poder identificarlo del resto, valida si ocurrio algún error, si no existe 
ningún error muestra el dato por consola, de haber error lo notifica

## inicio
Esta función se utiliza la función login para crear el token de autenticación 
luego hace una llamada a lista enviandole el token para poder mostrar los contactos
en consola, y por ultimo realiza un ciclo para crear 10 nuevos contactos.

